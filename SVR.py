import pandas
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn import svm
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt 
import math
lista=[]
lista.append("HomeTeam")	
lista.append("AwayTeam")
lista.append("gameBTW")		
lista.append("golo diff")
lista.append("golo diff2")
lista.append("JCasa")
lista.append("JFora")
#lista.append("golo diff3")	
lista.append("team1")	
lista.append("team2")	
import numpy as np
def computeCost(X, y, theta):  
    inner = np.power(((X * theta.T) - y), 2)
    return np.sum(inner) / (2 * len(X))


def gradientDescent(X, y, theta, alpha, iters):  
    temp = np.matrix(np.zeros(theta.shape))
    parameters =     int(theta.ravel().shape[1])
    cost = np.zeros(iters)

    for i in range(iters):
        error = (X * theta.T) - y

        for j in range(parameters):
            term = np.multiply(error, X[:,j])
            temp[0,j] = theta[0,j] - ((alpha / len(X)) * np.sum(term))

        theta = temp
        cost[i] = computeCost(X, y, theta)

    return theta, cost

games2 = pandas.read_csv("FF.csv")
inputVal= pandas.read_csv("input.csv")
ival= pandas.read_csv("val.csv")
#print games2
#print games2
numpyX=np.array(games2)
#print numpyX

# Print the names of the columns in g2.csvames.
# print(games2).shape[1]
# columns = games2.columns.tolist()
# #print np.matrix(numpyX[lista])
# y=lista

# X = np.matrix(games2[lista].values)  
# y2 = np.matrix(games2["vitoria"].values)  
# print y2
# theta = np.matrix(np.array([0,0,0,0,0,0]))

# print computeCost(X,y2,theta)
# alpha = 0.01  
# iters = 1000

# g, cost = gradientDescent(X, y2, theta, alpha, iters)
# g = np.matrix(np.where(np.isnan(g))[0]) 
# print g
# print computeCost(X,y2,g)


#train = games2.sample(frac=0.8, random_state=1)
#print train
#test = games2.loc[~games2.index.isin(train.index)]
#print train[columns]
#from sklearn import preprocessing as pre
#scaler = pre.StandardScaler().fit(train)
#X_train_scaled = scaler.transform(train)
#X_test_scaled = scaler.transform(test)
#y_train = scaler.fit_transform(y)




#SVR_model = svm.SVR(kernel='rbf',C=100,gamma=.001).fit(train[y],train["vitoria"])
#print test
#predict_y_array = SVR_model.predict(test[y])
#print predict_y_array


X_train, X_test, y_train, y_test = train_test_split(
        games2[lista], games2["vitoria"], test_size=0.00002 , random_state=34
     )




from sklearn.ensemble import RandomForestRegressor
from sklearn import metrics
from sklearn.metrics import confusion_matrix
# Initialize the model with some parameters.
#model = RandomForestRegressor(n_estimators=100, min_samples_leaf=10, random_state=1)
#model=LinearSVC()
from sklearn import linear_model 
import sklearn.svm
#model =linear_model.LinearRegression()  

from sklearn.neural_network import MLPClassifier
model= MLPClassifier(solver='lbfgs',activation='relu',learning_rate="adaptive",alpha=1e-7,hidden_layer_sizes=(17,), random_state=1,max_iter=350)
#model =clf = MLPClassifier(solver='lbfgs',  alpha=1e-7, hidden_layer_sizes=(9,), max_iter=100)
#model= svm.SVC(C=100,kernel = 'linear',probability=True,gamma=1)
#model= svm.LinearSVC()
#model= svm.SVC(C=1,kernel = 'linear',probability=True,gamma=1)
# Fit the model to the data.
model.fit(X_train,y_train)

# Make predictions.

#temp=[[15,16,1,0.784615385,1,0.5],[5,4,1,0.75862069,0.5,0.6],[6,1,0,0.828571429,0.1,0.3],[13,20,0,0.8,0.3,0.8]]
#temp = np.array(temp).reshape((1, -1))

temp=[]
for t in range(len(inputVal)):
    temp.append([inputVal[lista[0]][t],inputVal[lista[1]][t],inputVal[lista[2]][t],inputVal[lista[3]][t],inputVal[lista[4]][t],inputVal[lista[5]][t],inputVal[lista[6]][t],inputVal[lista[7]][t],inputVal[lista[8]][t]])


predictions = model.predict(temp)
# Compute the error.
# print"AAA", X_test
print "PREDITION ",predictions
from fuzzywuzzy import fuzz
ids=[]
for v in range(len(ival)):

    j=0
    i=None
    for t in range(len(inputVal)):
        if (fuzz.partial_ratio(ival["Home"][v].lower(),inputVal["ECasa"][t].lower()) + fuzz.partial_ratio(ival["Away"][v].lower(),inputVal["EFora"][t].lower()))>j and (fuzz.partial_ratio(ival["Home"][v].lower(),inputVal["ECasa"][t].lower()) + fuzz.partial_ratio(ival["Away"][v].lower(),inputVal["EFora"][t].lower())) >150:
            j=fuzz.partial_ratio(ival["Home"][v],inputVal["ECasa"][t]) + fuzz.partial_ratio(ival["Away"][v],inputVal["EFora"][t])
            i=v
    ids.append(i)

# print y_test
# arr=[]
# for x in y_test:
#     arr.append(x)
# #print arr
# #print X_test

# for x in X_test["team1"]:
#     print x
# cont=0
# for i in range(len(predictions)):
        
#         if arr[i] != predictions[i]:
#             print"--------------"
            
#             print "POS",i
#             print"Predict: ",predictions[i]
#             print"Suposto",arr[i]
#             print"--------------"
#             cont+=1
            

# print "ACERTOU", len(predictions)-cont 
# print len(predictions)
print(model.score(X_train, y_train))
import requests,json
arrP=model.predict_proba(np.array(temp).reshape(len(temp), -len(temp)))
print model.predict_proba(np.array(temp).reshape(len(temp), -len(temp)))
for p in range(len(inputVal)):
    if(predictions[p]==0):
        val="Victory"
    else:
        if(predictions[p]==1):
            val="Draw"
        else:
            val="Loss"
    #problablidades
    temp=float(arrP[p][0])
    arrP[p][0]=format(temp*100, '.2f') 
    temp=float(arrP[p][1])
    arrP[p][1]=format(temp*100, '.2f') 
    temp=float(arrP[p][2])
    arrP[p][2]=format(temp*100, '.2f') 
#    if ids[p]==None:
    #r=requests.post("http://13.59.175.203/api_send_secr.php/predict/", data=json.dumps({"league":"Siria","Home":(inputVal["ECasa"][p]).lower().title(),"Away":(inputVal["EFora"][p]).lower().title(),"HomeP": str(arrP[p][0])+"%","DrawP":str(arrP[p][1])+"%" ,"AwayP": str(arrP[p][2])+"%","predict": val}))
 #   else:
  #      r=requests.post("http://13.59.175.203/api_send_secr.php/predict/", data=json.dumps({"league":"Siria","Home":(inputVal["ECasa"][p]).lower().title(),"Away":(inputVal["EFora"][p]).lower().title(),"HomeP": str(arrP[p][0])+"%","DrawP":str(arrP[p][1])+"%" ,"AwayP": str(arrP[p][2])+"%","predict": val,"HomeODD":ival["OddV"][ids[p]],"DrawODD":ival["OddD"][ids[p]],"AwayODD":ival["OddL"][ids[p]]}))
#print r.content







#print games2.corr()["golo diff"]

#print test["vitoria"]
#print mean_squared_error(predictions, test[y])
#print(model.score(X_test, y_test))


#print(confusion_matrix(predictions, y_test))


# fig, ax = plt.subplots(figsize=(12,8))  
# ax.plot(X_test, predictions, 'r', label='Prediction')  
# ax.scatter(games2["golo diff"], games2["gameBTW"],games2["team1"],games2["team2"], label='Traning Data')  
# ax.legend(loc=2)  
# ax.set_xlabel('Population')  
# ax.set_ylabel('Profit')  
# ax.set_title('Predicted Profit vs. Population Size')  
# plt.show()
    