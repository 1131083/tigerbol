from __future__ import division

class Calculos:
	def __init__(self):
		self.calculadorVitoriaCasa=[]
		self.calculadorVitoriaFora=[]
		self.calculadorJogosCasa=[]
		self.calculadorJogosFora=[]
		self.calculadorEntreEquipas=[]
		self.pontosFinal=[]
		self.pontosFinalCasa_Fora=[]
		self.golosFinal=[]
		self.golosFinalCasa_Fora=[]

		self.pontosCasa=[]
		self.pontosFora=[]
		self.golosMCasa=[]
		self.golosMFora=[]
		self.golosSCasa=[]
		self.golosSFora=[]
		self.golosSFora=[]

	def jogosEntreEquipasD(self,keyH,keyA,arrayBTW):

		if(len(arrayBTW[keyH+"_"+keyA])==2):
			return (int(arrayBTW[str(keyH+"_"+keyA)][0])+int(arrayBTW[str(keyH+"_"+keyA)][1]))/4
		else:
			return (0.2)
	def jogosEntreEquipas2(self,games2,keyH,keyA,arrayBTW,g):
				
		if games2["AwayTeam"][g]==str(keyA) and games2["HomeTeam"][g]==str(keyH):

			if str(keyH+"_"+keyA) not in arrayBTW.keys():
				print "NOVO"
				arrayBTW[str(keyH+"_"+keyA)]=[]
			if str(keyA+"_"+keyH) not in arrayBTW.keys():
				print "NOVO"
				arrayBTW[str(keyA+"_"+keyH)]=[]
	
			if(len(arrayBTW[keyH+"_"+keyA])==3):
				self.calculadorEntreEquipas.append(((int(arrayBTW[str(keyH+"_"+keyA)][0])+int(arrayBTW[str(keyH+"_"+keyA)][1])+int(arrayBTW[str(keyH+"_"+keyA)][2]))/3)/3)
				print"CASA BTW :",arrayBTW[str(keyH+"_"+keyA)]
				arrayBTW[str(keyA+"_"+keyH)].pop(0)
				arrayBTW[str(keyH+"_"+keyA)].pop(0)
			else:
				self.calculadorEntreEquipas.append(0.5)
			

			#maximo 5
	 		
			if(games2["Vitoria"][g]=="0"):
				arrayBTW[str(keyH+"_"+keyA)].insert(3,"3")
				arrayBTW[str(keyA+"_"+keyH)].insert(3,"0")
			

			if(games2["Vitoria"][g]=="1"):
				arrayBTW[str(keyH+"_"+keyA)].insert(3,"1")
				arrayBTW[str(keyA+"_"+keyH)].insert(3,"1")
			
			if(games2["Vitoria"][g]=="2"):
				arrayBTW[str(keyH+"_"+keyA)].insert(3,"0")
				arrayBTW[str(keyA+"_"+keyH)].insert(3,"3")
		return arrayBTW
	def jogosEntreEquipas(self,games2,keyH,keyA,arrayBTW,g):
				
		if games2["AwayTeam"][g]==str(keyA) and games2["HomeTeam"][g]==str(keyH):

			if str(keyH+"_"+keyA) not in arrayBTW.keys():
				print "NOVO"
				arrayBTW[str(keyH+"_"+keyA)]=[]
			if str(keyA+"_"+keyH) not in arrayBTW.keys():
				print "NOVO"
				arrayBTW[str(keyA+"_"+keyH)]=[]
	
			if(len(arrayBTW[keyH+"_"+keyA])==3):
				self.calculadorEntreEquipas.append((int(arrayBTW[str(keyH+"_"+keyA)][0])+int(arrayBTW[str(keyH+"_"+keyA)][1])+int(arrayBTW[str(keyH+"_"+keyA)][2]))/6)

				arrayBTW[str(keyA+"_"+keyH)].pop(0)
				arrayBTW[str(keyH+"_"+keyA)].pop(0)
			else:
				self.calculadorEntreEquipas.append(0.5)
			
	 		
			if(games2["Vitoria"][g]=="0"):
				arrayBTW[str(keyH+"_"+keyA)].insert(3,"2")
				arrayBTW[str(keyA+"_"+keyH)].insert(3,"0")
			

			if(games2["Vitoria"][g]=="1"):
				arrayBTW[str(keyH+"_"+keyA)].insert(3,"1")
				arrayBTW[str(keyA+"_"+keyH)].insert(3,"1")
			
			if(games2["Vitoria"][g]=="2"):
				arrayBTW[str(keyH+"_"+keyA)].insert(3,"0")
				arrayBTW[str(keyA+"_"+keyH)].insert(3,"2")
			

				
		return arrayBTW
	def pontosCalculo(self,pCasa,pFora,nJogosCasa,nJogosFora):

		if nJogosCasa==0:
			nJogosCasa=1
		if nJogosFora==0:
			nJogosFora=1

		self.pontosFinal.append(((pCasa/nJogosCasa)-(pFora/nJogosFora))/3)


	def pontosPorEquipa(self,pCasa,pFora,nJogosCasa,nJogosFora):
		if nJogosCasa==0:
			nJogosCasa=1
		if nJogosFora==0:
			nJogosFora=1

		self.pontosCasa.append(pCasa/nJogosCasa)
		self.pontosFora.append(pFora/nJogosFora)
		self.pontosFinalCasa_Fora.append(((pCasa/nJogosCasa)-(pFora/nJogosFora))/3)

	def golosCalculo(self,gCasaM,gCasaS,gForaM,gForaS,nJogosCasa,nJogosFora):
		dfCasa=abs(gCasaM)-abs(gCasaS)
		dfFora=abs(gForaM)-abs(gForaS)
		print "dfCasa",dfCasa
		print "dfFora",dfFora
		if(dfCasa==0 and dfFora==0):
			self.golosFinal.append(0)
		else:
			if(abs(dfCasa)>=abs(dfFora)):
				print "res ",(dfCasa-dfFora)/dfCasa
				self.golosFinal.append((abs(dfCasa)-abs(dfFora))/dfCasa)
					
			else:
				print "res ",(dfFora-dfCasa)/dfFora
				self.golosFinal.append((abs(dfFora)-abs(dfCasa))/dfFora)
					

		
	def golosMarcados(self,gCasa,gFora,nJogosCasa,nJogosFora):
		self.golosMCasa.append(gCasa/nJogosCasa)
		self.golosMFora.append(gFora/nJogosFora)	

	def golosSofridos(self,gCasa,gFora,nJogosCasa,nJogosFora):
		self.golosSCasa.append(gCasa/nJogosCasa)
		self.golosSFora.append(gFora/nJogosFora)
