import pandas
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
# Read in the data.
games = pandas.read_csv("premierLeague.csv")
# Print the names of the columns in games.
print(games.columns)

print(games.shape)

import matplotlib.pyplot as plt

# Make a histogram of all the ratings in the average_rating column.
#plt.hist(games["FTHG"])

# Show the plot.
#plt.show()


from sklearn.cluster import KMeans

# Initialize the model with 2 parameters -- number of clusters and random state.
kmeans_model = KMeans(n_clusters=5, random_state=1)
# Get only the numeric columns from games.
good_columns = games._get_numeric_data()
print good_columns

# Fit the model using the good columns.
kmeans_model.fit(good_columns)
# Get the cluster assignments.
labels = kmeans_model.labels_

print labels


#from sklearn.decomposition import PCA

# Create a PCA model.
#pca_2 = PCA(2)
# Fit the PCA model on the numeric columns from earlier.
#plot_columns = pca_2.fit_transform(good_columns)
# Make a scatter plot of each game, shaded according to cluster assignment.
#plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1], c=labels)
# Show the plot.
#plt.show()

print games.corr()["BbAv<2.5"]

columns = games.columns.tolist()
# Filter the columns to remove ones we don't want.
columns = [c for c in columns if c not in ["Div", "Date","FTR", "HomeTeam", "AwayTeam","HTR","Referee"]]

# Store the variable we'll be predicting on.
target = "FTHG"


# Import a convenience function to split the sets.
from sklearn.model_selection  import train_test_split

# Generate the training set.  Set random_state to be able to replicate results.
train = games.sample(frac=0.8, random_state=1)
# Select anything not in the training set and put it in the testing set.
test = games.loc[~games.index.isin(train.index)]
# Print the shapes of both sets.
print(train.shape)
print(test.shape)

from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression

# Initialize the model class.
model = LinearRegression(copy_X=True, fit_intercept=True, normalize=False)
#Fit the model to the training data.
model.fit(train[columns], train[target])

predictions = model.predict(test[columns])
print predictions
print mean_squared_error(predictions, test[target])


# Import the random forest model.
from sklearn.ensemble import RandomForestRegressor
from sklearn import metrics
# Initialize the model with some parameters.
model = RandomForestRegressor(n_estimators=100, min_samples_leaf=10, random_state=1)
# Fit the model to the data.
model.fit(train[columns], train[target])
# Make predictions.

predictions = model.predict(test[columns])
# Compute the error.

print predictions
print mean_squared_error(predictions, test[target])


