# -*- coding: utf-8 -*-

import scrapy

class GameItem(scrapy.Item):
	oddV = scrapy.Field()
	oddD = scrapy.Field()
	oddL = scrapy.Field()
	oddNameH = scrapy.Field()
	oddNameA = scrapy.Field()
	oddData = scrapy.Field()
	#homeTeam_result = scrapy.Field()
