# -*- coding: utf-8 -*-

import scrapy

class GameItem(scrapy.Item):
	table_Year = scrapy.Field()
	homeTeam_name = scrapy.Field()
	awayTeam_name = scrapy.Field()
	homeTeam_goals_FH = scrapy.Field()
	homeTeam_goals_FT = scrapy.Field()
	awayTeam_goals_FH = scrapy.Field()
	awayTeam_goals_FT = scrapy.Field()
	jornada = scrapy.Field()
	data = scrapy.Field()
	#homeTeam_result = scrapy.Field()
