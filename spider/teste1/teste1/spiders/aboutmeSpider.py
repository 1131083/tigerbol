# -*- coding: utf-8 -*-

from __future__ import unicode_literals


import scrapy, logging, re, unicodedata #MySQLdb

from scrapy.utils.response import open_in_browser as view
from scrapy.spiders import Spider, CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from teste1.items import *

from unidecode import unidecode
import time

from scrapy import Selector
import requests
import time
from multiprocessing import Process
import regex
#scrapy crawl aboutmeSpider # LOG_ENABLED=0

class aboutmeSpider(Spider):
	name = 'aboutmeSpider'
	home_uri = 'http://www.xscores.com/'
	start_urls = [home_uri]
	listCont=0
	def __init__(self, country=None, city=None, query=None, *args, **kwargs):
		super(aboutmeSpider, self).__init__(*args, **kwargs)
		#self.edicoes = ["2016-2017"]
		self.edicoes = ["2012-2013","2013-2014","2014-2015","2015-2016","2016-2017","2017-2018"]
		self.listCont=0
	def parse(self, response):
		""" Yields a request to job-offers listing """
		#104349
		edicoes = ["2012-2013","2013-2014","2014-2015","2015-2016","2016-2017","2017-2018"]
		#edicoes = ["2016-2017"]
		
		for edicao in edicoes:
			for i in reversed(range(1,13)):
			#for i in ["stage 1","stage 2","round 16","quarter finals","semi finals","final"]:
			#for i in ["first qualifying round","second qualifying round","third qualifying round","play off round","group stage","round 32","round 16","quarter finals","semi finals","final"]:	
				 
				#url ="http://www.xscores.com/soccer/cupresults/europe_(uefa)/uefa_europa_league/%s/%s" % (edicao,i)
				#url ='http://www.xscores.com/soccer/leagueresults/china/super_league/%s/p/%d' % (edicao,i)
				#url ='http://www.xscores.com/soccer/leagueresults/australia/victoria/%s/p/%d' % (edicao,i)
				#url = 'http://www.xscores.com/soccer/leagueresults/japan/premier_league/%s/p/%d' % (edicao,i)	
				#url = 'http://www.xscores.com/soccer/leagueresults/japan/j2_league/%s/p/%d' % (edicao,i)	
				#url = 'http://www.xscores.com/soccer/leagueresults/hungary/division_1/%s/p/%d' % (edicao,i)
				#url = 'http://www.xscores.com/soccer/leagueresults/mexico/super_league/%s/p/%d' % (edicao,i)
				url ="http://www.xscores.com/soccer/leagueresults/brazil/liga_mx/%s/p/%d" % (edicao,i)
				#url = 'http://www.xscores.com/soccer/leagueresults/russia/super_league/%s/p/%d' % (edicao,i)
				#url = 'http://www.xscores.com/soccer/leagueresults/colombia/clausura/%s/p/%d' % (edicao,i)
				#url = 'http://www.xscores.com/soccer/leagueresults/syria/premier_league/%s/p/%d' % (edicao,i)
				#url ="http://www.xscores.com/soccer/leagueresults/iran/premier_league/%s/p/%d" % (edicao,i)
				#url ="http://www.xscores.com/soccer/leagueresults/south_korea/premier_league/%s/p/%d" % (edicao,i)
				#url ="http://www.xscores.com/soccer/leagueresults/iran/premier_league/%s/p/%d" % (edicao,i)
				print(url)
				self.listCont+=1
				#time.sleep(100)
				yield scrapy.Request(url,cookies={'store_language':'en'},callback= self.parse_info)

	def parse_info(self,response):
		print("parse_info")
		""" job offers """
		items = []

		item = GameItem()
		item['table_Year'] = ''
		item['homeTeam_name'] = ''
		item['awayTeam_name'] = ''
		item['homeTeam_goals_FH'] = ''
		item['homeTeam_goals_FT'] = ''
		item['awayTeam_goals_FH'] = ''
		item['awayTeam_goals_FT'] = ''
		item['jornada'] = ''
#//*[@id="scoretable"]/tr[3]/td[5]
		hname=[]
		tF=[]
		pPH=[]
		pPA=[]
		VTFH=[]
		sPH=[]
		sPA=[]
		size = len(response.xpath('//*[@id="scoretable"]/tr'))+1
		valida=True
		print "size---->",size
		arrData=[]
		xv=""

		for val in range(2,size):
			teamH=response.xpath('//*[@id="scoretable"]/tr[%s]/td[5]/text()' % (str(val))).extract()
			teamA=response.xpath('//*[@id="scoretable"]/tr[%s]/td[8]/text()' % (str(val))).extract()
			resTHalf=response.xpath('//*[@id="scoretable"]/tr[%s]/td[11]/text()' % (str(val))).extract()
			resFullTime=response.xpath('//*[@id="scoretable"]/tr[%s]/td[12]/text()' % (str(val))).extract()
			#resFullTime=response.xpath('//*[@id="scoretable"]/tr[%s]/td[12]/b/text()' % (str(val))).extract()
			print resFullTime
			

			if len(resFullTime)>0 and len(teamH)>0 and len(teamA)>0 and len(resTHalf)>0:
				r=""
				arrData.append(data)
				print "ANTES ",resTHalf
				for i in resTHalf:
					r=str(i)
				print "DEPOIS ",r
				
				try:
					sp=r.split("-")
					pPH.append(int(sp[0].strip()))
					pPA.append(int(sp[1].strip()))
					sp[0]=int(sp[0].strip())
					sp[1]=int(sp[1].strip())
				except Exception as e:
					valida=False
					pass
				
				if len(resFullTime)>0 and valida==True:
					
					for i in resFullTime:
						r=str(i)
					try:
						spF=str(r).split("-")
					except Exception as e:
						pass
					#print "AAA",spF[0]
					if (int(spF[0])-int(sp[0]))>0:
						segP=(int(spF[0])-int(sp[0]))
						sPH.append(segP)
					else:
						if (int(spF[0])-int(sp[0]))==0:
							segP=(int(spF[0])-int(sp[0]))
							sPH.append(segP)
						else:
							if (int(spF[0])-int(sp[0]))<0:
								segP=(int(spF[0])-int(sp[0]))*-1
								sPH.append(segP)
					if (int(spF[1])-int(sp[1]))>0:
						segP2=(int(spF[1])-int(sp[1]))
						sPA.append(segP2)
					else:
						if (int(spF[1])-int(sp[1]))==0:
							segP2=(int(spF[1])-int(sp[1]))
							sPA.append(segP2)	
						else:
							if int(spF[1])-int(sp[1])<0:
								segP2=(int(spF[1])-int(sp[1]))*-1
								sPA.append(segP2)
							
			else:
				data=response.xpath('//*[@id="scoretable"]/tr[%s]/td/text()' % (str(val))).extract()							
			if len(teamH)>0:
				hname.append(teamH)
			if len(teamA)>0:
				tF.append(teamA)
		#print hname
		print "response url",response.url
		for yr in self.edicoes:	
			searchObj = regex.getLeague(yr,response.url)		
			if searchObj is not None:
				item['table_Year'] = searchObj
				print "ANO",item['table_Year']
			print "JORNADA",(response.url.split("/"))[len(response.url.split("/"))-1]
			item['jornada'] = (response.url.split("/"))[len(response.url.split("/"))-1]
		if hname is not None :
			#hname = hname.strip()
			item['homeTeam_name'] = hname
			item['awayTeam_name'] = tF
			item['homeTeam_goals_FH'] = pPH
			item['homeTeam_goals_FT'] = sPH
			item['awayTeam_goals_FH'] = pPA
			item['awayTeam_goals_FT'] = sPA
			item['data']=arrData
			#print("hname")
			#print(hname)

		
		
		items.append(item)
				

		#print(":............................................:")
		#print(":............................................:")
		#print(item)
		#print(":............................................:")
		#print(":............................................:")

		return items